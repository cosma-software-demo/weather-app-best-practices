import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sun_day/locations/locations_weather_api.dart';

import 'locations_weather_api_test.mocks.dart';

@GenerateMocks([
  LocationWeatherModelMapper,
  Dio,
])
void main() {
  group('Locations weather api', () {
    late MockLocationWeatherModelMapper mapper;
    late MockDio dio;

    late LocationsWeatherApi locationsWeatherApi;

    setUpAll(() async {
      TestWidgetsFlutterBinding.ensureInitialized();

      dio = MockDio();
      mapper = MockLocationWeatherModelMapper();

      locationsWeatherApi = LocationsWeatherApi(mapper, dio);
    });

    setUp(() async {});

    tearDown(() {});

    test("getLocationWeatherByCityName returns correct data", () async {
      const cityName = 'testCity';
      const responseData = {'data': 'data'};
      Response response = Response(data: responseData, requestOptions: RequestOptions());
      const mapperResponse = LocationWeatherModel(
        name: 'name',
        icon: 'icon',
        currentTemp: 'currentTemp',
        minTemp: 'minTemp',
        maxTemp: 'maxTemp',
      );
      // Arrange
      when(dio.get(any)).thenAnswer((_) => Future.value(response));
      when(mapper.map(responseData)).thenReturn(mapperResponse);

      // Act
      final result = await locationsWeatherApi.getLocationWeatherByCityName(cityName);

      // Assert
      expect(result, equals(mapperResponse));

      verify(dio.get(any)).called(1);
      verify(mapper.map(responseData)).called(1);
    });

    test("getLocationWeatherByCityName throw error", () async {
      const cityName = 'testCity';
      const responseData = {'data': 'data'};
      Response response = Response(data: responseData, requestOptions: RequestOptions());
      // Arrange
      when(dio.get(any)).thenAnswer((_) => Future.value(response));
      when(mapper.map(responseData)).thenThrow(Exception());

      // Act Assert
      expect(() async => await locationsWeatherApi.getLocationWeatherByCityName(cityName),
          throwsA(isA<LocationsWeatherApiException>()));
    });
  });
}
