# sun_day

Weather app

## Getting Started

To run this project use the following command: flutter run lib/main_prod.dart --dart-define=weatherApiKey=API_KEY --dart-define=openWeatherApiKey=API_KEY

You can get weatherApiKey from https://www.weatherapi.com/ 

You can get openWeatherApiKey from https://api.openweathermap.org/

Generate code with: flutter packages pub run build_runner build --delete-conflicting-outputs

This project contains best practices for: 
- Storing sensitive information in environment variables,
- Dependency injection,
- State management with Cubit,
- Unit Testing,
- Generating code. 

