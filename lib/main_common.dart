import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:sun_day/core/app_navigator_provider.dart';
import 'package:sun_day/core/di/injector.dart';
import 'locations/locations_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData.light(
        useMaterial3: true,
      ),
      darkTheme: ThemeData.dark(
        useMaterial3: true,
      ),
      routerConfig: getIt.get<AppNavigatorProvider>().provideRouter(),
    );
  }
}
