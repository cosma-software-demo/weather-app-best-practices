import 'package:flutter/material.dart';
import 'package:sun_day/weather_details/current_weather_widget.dart';
import 'package:sun_day/weather_details/following_days_weather_widget.dart';
import 'package:sun_day/weather_details/today_hourly_weather_widget.dart';

class WeatherDetailsPage extends StatelessWidget {
  static const kRouteParamsLocation = 'location';
  static const kRouteName = 'location';
  static const kRoutePath = ':$kRouteParamsLocation';

  final String location;
  const WeatherDetailsPage({required this.location, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 50,
            child: CurrentWeatherWidget(location: location),
          ),
          Expanded(
            flex: 18,
            child: TodayHourlyWeatherWidget(location: location),
          ),
          Expanded(
            flex: 32,
            child: FollowingDaysWeatherWidget(location: location),
          ),
        ],
      ),
    );
  }
}
