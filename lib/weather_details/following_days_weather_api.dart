import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

@Injectable()
class FollowingDaysWeatherApi {
  static const apiKey = String.fromEnvironment('weatherApiKey', defaultValue: '');
  final Location location;
  final Dio dio;
  final MultipleDaysWeatherMapper mapper;

  FollowingDaysWeatherApi(this.location, this.dio, this.mapper);

  Future<List<DayWeatherModel>> fetchWeatherByCurrentLocation(int days) async {
    try {
      bool serviceEnabled;
      PermissionStatus permissionGranted;
      LocationData locationData;

      serviceEnabled = await location.serviceEnabled();
      if (!serviceEnabled) {
        serviceEnabled = await location.requestService();
        if (!serviceEnabled) {
          // Enable service
        }
      }

      permissionGranted = await location.hasPermission();
      if (permissionGranted == PermissionStatus.denied) {
        permissionGranted = await location.requestPermission();
        if (permissionGranted != PermissionStatus.granted) {
          // Perm not granted
        }
      }

      locationData = await location.getLocation();

      final double lat = locationData.latitude ?? 44.4323;
      final double lon = locationData.longitude ?? 26.1063;

      final response = await dio.get(
          'http://api.weatherapi.com/v1/forecast.json?key=$apiKey&q=$lat,$lon&days=$days&aqi=no&alerts=no&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw FollowingDaysWeatherApiException();
    }
  }

  Future<List<DayWeatherModel>> fetchWeatherByCityName(String cityName, int days) async {
    try {
      final response = await dio.get(
          'http://api.weatherapi.com/v1/forecast.json?key=$apiKey&q=$cityName&days=$days&aqi=no&alerts=no&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw FollowingDaysWeatherApiException();
    }
  }
}

@Injectable()
class MultipleDaysWeatherMapper {
  List<DayWeatherModel> map(Map<String, dynamic> apiResponseBody) {
    final List<DayWeatherModel> weatherModels = List.from([], growable: true);
    final entries = apiResponseBody['forecast']['forecastday'] as List? ?? [];
    final DateFormat formatter = DateFormat('E');

    entries.forEach(
      (element) {
        final DateTime hour = DateTime.fromMillisecondsSinceEpoch(
          element['date_epoch'] * 1000,
        );

        weatherModels.add(
          DayWeatherModel(
            dayName: formatter.format(hour),
            icon: 'http:${element['day']['condition']['icon']}',
            minTemp: '${(element['day']['mintemp_c'] as num).toString()}°',
            maxTemp: '${(element['day']['maxtemp_c'] as num).toString()}°',
          ),
        );
      },
    );

    return weatherModels;
  }
}

class DayWeatherModel {
  final String dayName;
  final String icon;
  final String minTemp;
  final String maxTemp;

  DayWeatherModel({
    required this.dayName,
    required this.icon,
    required this.minTemp,
    required this.maxTemp,
  });
}

class FollowingDaysWeatherApiException implements Exception {}
