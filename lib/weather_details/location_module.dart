import 'package:injectable/injectable.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
abstract class LocationModule {
  Location get location {
    return Location();
  }
}
