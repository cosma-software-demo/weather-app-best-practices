import 'package:flutter/material.dart';
import 'package:sun_day/core/di/injector.dart';
import 'package:sun_day/weather_details/today_weather_api.dart';

class TodayHourlyWeatherWidget extends StatelessWidget {
  final String location;

  const TodayHourlyWeatherWidget({required this.location, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<HourWeatherModel>>(
      future: getIt<TodayWeatherApi>().fetchTodayWeatherByCityName(location),
      builder: (BuildContext context, AsyncSnapshot<List<HourWeatherModel>> snapshot) {
        if (snapshot.hasData) {
          final weatherData = snapshot.data!;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 16.0),
                child: Text(
                  'Today weather',
                  textAlign: TextAlign.left,
                ),
              ),
              Expanded(
                child: ListView.separated(
                  padding: const EdgeInsets.only(left: 16, right: 16),
                  itemCount: weatherData.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(weatherData[index].hour),
                          const SizedBox(
                            width: 4,
                          ),
                          Image.network(
                            weatherData[index].icon,
                            height: 45,
                            width: 45,
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Text(weatherData[index].temperature),
                          const SizedBox(
                            width: 4,
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 24,
                    );
                  },
                ),
              ),
              const Divider(),
            ],
          );
        } else {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
      },
    );
  }
}
