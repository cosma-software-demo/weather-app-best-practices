import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
abstract class DateFormatModule {
  DateFormat get dateFormat {
    return DateFormat('jm');
  }
}
