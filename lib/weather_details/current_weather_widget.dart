import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sun_day/core/di/injector.dart';
import 'package:sun_day/weather_details/current_weather_api.dart';

class CurrentWeatherWidget extends StatelessWidget {
  final String location;

  const CurrentWeatherWidget({required this.location, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CurrentWeatherModel>(
      future: getIt<WeatherApi>().fetchCurrentWeatherByCityName(location),
      builder:
          (BuildContext context, AsyncSnapshot<CurrentWeatherModel> snapshot) {
        if (snapshot.hasData) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 16,
                    right: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const Spacer(),
                      Expanded(
                        child: Text(
                          snapshot.data!.cityName,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 24,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          snapshot.data!.temperature,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 32,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          snapshot.data!.weatherDescription,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  const Icon(MdiIcons.water),
                                  const SizedBox(
                                    width: 4,
                                  ),
                                  Text(snapshot.data!.humidity),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(MdiIcons.information),
                                  const SizedBox(
                                    width: 4,
                                  ),
                                  Text(snapshot.data!.pressure),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  const Icon(MdiIcons.weatherWindy),
                                  const SizedBox(
                                    width: 4,
                                  ),
                                  Text(snapshot.data!.windSpeed),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                const Text('Sunrise'),
                                const SizedBox(
                                  height: 4,
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    const Icon(MdiIcons.weatherSunsetUp),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Text(snapshot.data!.sunrise),
                                  ],
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                const Text('Sunset'),
                                const SizedBox(
                                  height: 4,
                                ),
                                Row(
                                  children: [
                                    const Icon(MdiIcons.weatherSunsetDown),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Text(snapshot.data!.sunset),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const Divider(),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
