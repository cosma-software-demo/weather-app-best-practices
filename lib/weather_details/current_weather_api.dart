import 'dart:core';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

@Injectable()
class WeatherApi {
  final Location location;
  final Dio dio;
  final CurrentWeatherMapper mapper;

  static const apiKey = String.fromEnvironment('openWeatherApiKey', defaultValue: '');

  WeatherApi(this.location, this.dio, this.mapper);

  Future<CurrentWeatherModel> fetchCurrentWeatherByCurrentLocation() async {
    try {
      bool serviceEnabled;
      PermissionStatus permissionGranted;
      LocationData locationData;

      serviceEnabled = await location.serviceEnabled();
      if (!serviceEnabled) {
        serviceEnabled = await location.requestService();
        if (!serviceEnabled) {
          // Enable service
        }
      }

      permissionGranted = await location.hasPermission();
      if (permissionGranted == PermissionStatus.denied) {
        permissionGranted = await location.requestPermission();
        if (permissionGranted != PermissionStatus.granted) {
          // Perm not granted
        }
      }

      locationData = await location.getLocation();

      final double lat = locationData.latitude ?? 44.4323;
      final double lon = locationData.longitude ?? 26.1063;

      final response =
          await dio.get('https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=$apiKey&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw WeatherApiException();
    }
  }

  Future<CurrentWeatherModel> fetchCurrentWeatherByCityName(String cityName) async {
    try {
      final response =
          await dio.get('https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$apiKey&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw WeatherApiException();
    }
  }
}

class CurrentWeatherModel {
  final String cityName;
  final String temperature;
  final String weatherDescription;
  final String humidity;
  final String pressure;
  final String windSpeed;
  final String sunrise;
  final String sunset;

  CurrentWeatherModel({
    required this.cityName,
    required this.temperature,
    required this.weatherDescription,
    required this.humidity,
    required this.pressure,
    required this.windSpeed,
    required this.sunrise,
    required this.sunset,
  });
}

@Injectable()
class CurrentWeatherMapper {
  final DateFormat formatter;

  CurrentWeatherMapper(this.formatter);

  CurrentWeatherModel map(Map<String, dynamic> apiResponseBody) {
    final DateTime sunrise = DateTime.fromMillisecondsSinceEpoch(
      apiResponseBody['sys']['sunrise'] * 1000,
    );
    final DateTime sunset = DateTime.fromMillisecondsSinceEpoch(
      apiResponseBody['sys']['sunset'] * 1000,
    );

    return CurrentWeatherModel(
      cityName: apiResponseBody['name'],
      temperature: '${(apiResponseBody['main']['temp'] as num).toString()}°',
      weatherDescription: apiResponseBody['weather'].first['main'].toString(),
      humidity: '${(apiResponseBody['main']['humidity'] as num).toString()}%',
      pressure: '${(apiResponseBody['main']['pressure'] as num).toString()} hPa',
      windSpeed: '${(apiResponseBody['wind']['speed'] as num).toString()} m/s',
      sunrise: formatter.format(sunrise),
      sunset: formatter.format(sunset),
    );
  }
}

class WeatherApiException implements Exception {}
