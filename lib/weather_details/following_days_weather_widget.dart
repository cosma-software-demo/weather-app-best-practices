import 'package:flutter/material.dart';
import 'package:sun_day/core/di/injector.dart';
import 'package:sun_day/weather_details/following_days_weather_api.dart';

class FollowingDaysWeatherWidget extends StatelessWidget {
  final String location;

  const FollowingDaysWeatherWidget({required this.location, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<DayWeatherModel>>(
      future: getIt<FollowingDaysWeatherApi>().fetchWeatherByCityName(location, 3),
      builder: (BuildContext context, AsyncSnapshot<List<DayWeatherModel>> snapshot) {
        if (snapshot.hasData) {
          final weatherData = snapshot.data!;
          return ListView.builder(
            padding: const EdgeInsets.only(left: 16, right: 16),
            itemCount: weatherData.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      weatherData[index].dayName,
                      style: const TextStyle(
                        fontSize: 24,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Image.network(
                      weatherData[index].icon,
                      height: 60,
                      width: 60,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          weatherData[index].maxTemp,
                          style: const TextStyle(
                            fontSize: 24,
                          ),
                        ),
                        Text(
                          weatherData[index].minTemp,
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            },
          );
        } else {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
      },
    );
  }
}
