import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

@Injectable()
class TodayWeatherApi {
  static const apiKey = String.fromEnvironment('weatherApiKey', defaultValue: '');
  final Location location;
  final Dio dio;
  final TodayWeatherMapper mapper;

  TodayWeatherApi(this.location, this.dio, this.mapper);

  Future<List<HourWeatherModel>> fetchTodayWeatherByCurrentLocation() async {
    try {
      bool serviceEnabled;
      PermissionStatus permissionGranted;
      LocationData locationData;

      serviceEnabled = await location.serviceEnabled();
      if (!serviceEnabled) {
        serviceEnabled = await location.requestService();
        if (!serviceEnabled) {
          // Enable service
        }
      }

      permissionGranted = await location.hasPermission();
      if (permissionGranted == PermissionStatus.denied) {
        permissionGranted = await location.requestPermission();
        if (permissionGranted != PermissionStatus.granted) {
          // Perm not granted
        }
      }

      locationData = await location.getLocation();

      final double lat = locationData.latitude ?? 44.4323;
      final double lon = locationData.longitude ?? 26.1063;

      final response = await dio.get(
          'http://api.weatherapi.com/v1/forecast.json?key=$apiKey&q=$lat,$lon&days=1&aqi=no&alerts=no&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw TodayWeatherApiException();
    }
  }

  Future<List<HourWeatherModel>> fetchTodayWeatherByCityName(String cityName) async {
    try {
      final response = await dio.get(
          'http://api.weatherapi.com/v1/forecast.json?key=$apiKey&q=$cityName&days=1&aqi=no&alerts=no&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw TodayWeatherApiException();
    }
  }
}

@Injectable()
class TodayWeatherMapper {
  List<HourWeatherModel> map(Map<String, dynamic> apiResponseBody) {
    final List<HourWeatherModel> weatherModels = List.from([], growable: true);
    final entries = (apiResponseBody['forecast']['forecastday'] as List? ?? []).first['hour'] ?? [];
    final DateFormat formatter = DateFormat('j');

    entries.forEach(
      (element) {
        final DateTime hour = DateTime.fromMillisecondsSinceEpoch(
          element['time_epoch'] * 1000,
        );

        weatherModels.add(
          HourWeatherModel(
            hour: formatter.format(hour),
            icon: 'http:${element['condition']['icon']}',
            temperature: '${(element['temp_c'] as num).toString()}°',
          ),
        );
      },
    );

    return weatherModels;
  }
}

class HourWeatherModel {
  final String hour;
  final String icon;
  final String temperature;

  HourWeatherModel({
    required this.hour,
    required this.icon,
    required this.temperature,
  });
}

class TodayWeatherApiException implements Exception {}
