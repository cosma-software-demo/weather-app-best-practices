import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:sun_day/locations/locations_repository.dart';
import 'package:sun_day/locations/locations_weather_api.dart';

@Injectable()
class LocationsPageCubit extends Cubit<LocationsPageState> {
  final LocationsRepository _repository;

  LocationsPageCubit(this._repository) : super(const LocationsPageInitialState(LocationsPageUiModel([], true)));

  void onInitState() async {
    try {
      final locations = await _repository.getLocations();
      emit(
        LocationsPageLoadedState(
          state.uiModel.copyWith(
            locations: locations,
          ),
        ),
      );
    } catch (e) {
      emit(LocationsPageLoadedState(state.uiModel));
      // Emit LocationsPageDisplayMessageState with a custom message based on the error type
    }
  }

  void onAddNewCity(String cityName) async {
    try {
      final location = await _repository.getLocation(cityName);

      emit(LocationsPageLoadingState(state.uiModel));

      final newLocations = List.of(state.uiModel.locations)..add(location);

      emit(
        LocationsPageLoadedState(state.uiModel.copyWith(locations: newLocations)),
      );

      await _repository.saveNewLocationName(cityName);
    } catch (e) {
      // Emit LocationsPageDisplayMessageState with a custom message based on the error type
    }
  }
}

abstract class LocationsPageState extends Equatable {
  final LocationsPageUiModel uiModel;

  const LocationsPageState(this.uiModel);
}

class LocationsPageInitialState extends LocationsPageState {
  const LocationsPageInitialState(LocationsPageUiModel uiModel) : super(uiModel);

  @override
  List<Object?> get props => [uiModel];
}

class LocationsPageLoadingState extends LocationsPageState {
  const LocationsPageLoadingState(LocationsPageUiModel uiModel) : super(uiModel);

  @override
  List<Object?> get props => [uiModel];
}

class LocationsPageLoadedState extends LocationsPageState {
  const LocationsPageLoadedState(LocationsPageUiModel uiModel) : super(uiModel);

  @override
  List<Object?> get props => [uiModel];
}

class LocationsPageDisplayMessageState extends LocationsPageState {
  const LocationsPageDisplayMessageState(LocationsPageUiModel uiModel) : super(uiModel);

  @override
  List<Object?> get props => [uiModel];
}

class LocationsPageUiModel extends Equatable {
  final bool hasInternetConnection;
  final List<LocationWeatherModel> locations;

  const LocationsPageUiModel(this.locations, this.hasInternetConnection);

  LocationsPageUiModel copyWith({List<LocationWeatherModel>? locations, bool? hasInternetConnection}) =>
      LocationsPageUiModel(
        locations ?? this.locations,
        hasInternetConnection ?? this.hasInternetConnection,
      );

  @override
  List<Object?> get props => [
        locations,
        hasInternetConnection,
      ];
}
