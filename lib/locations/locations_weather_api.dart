import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class LocationsWeatherApi {
  static const apiKey = String.fromEnvironment('weatherApiKey', defaultValue: '');
  final LocationWeatherModelMapper mapper;
  final Dio dio;

  LocationsWeatherApi(this.mapper, this.dio);

  Future<LocationWeatherModel> getLocationWeatherByCityName(String cityName) async {
    try {
      final response = await dio.get(
          'http://api.weatherapi.com/v1/forecast.json?key=$apiKey&q=$cityName&days=1&aqi=no&alerts=no&units=metric');
      final Map<String, dynamic> apiResponseBody = response.data;
      return mapper.map(apiResponseBody);
    } catch (e) {
      print(e);
      throw LocationsWeatherApiException();
    }
  }
}

@Injectable()
class LocationWeatherModelMapper {
  LocationWeatherModel map(Map<String, dynamic> apiResponseBody) {
    final entries = apiResponseBody['forecast']['forecastday'] as List? ?? [];

    final element = entries.first;

    return LocationWeatherModel(
      name: apiResponseBody['location']['name'],
      currentTemp: '${apiResponseBody['current']['temp_c'].toString()}°',
      icon: 'http:${element['day']['condition']['icon']}',
      minTemp: '${(element['day']['mintemp_c'] as num).toString()}°',
      maxTemp: '${(element['day']['maxtemp_c'] as num).toString()}°',
    );
  }
}

class LocationWeatherModel extends Equatable {
  final String name;
  final String icon;
  final String currentTemp;
  final String minTemp;
  final String maxTemp;

  const LocationWeatherModel({
    required this.name,
    required this.icon,
    required this.currentTemp,
    required this.minTemp,
    required this.maxTemp,
  });

  @override
  List<Object?> get props => [
        name,
        icon,
        currentTemp,
        minTemp,
        maxTemp,
      ];
}

class LocationsWeatherApiException implements Exception {}
