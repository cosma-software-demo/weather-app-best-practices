import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sun_day/locations/locations_weather_api.dart';

@Injectable()
class LocationsRepository {
  final LocationsWeatherApi api;
  final SharedPreferencesDataStore preferencesDataStore;

  LocationsRepository(this.api, this.preferencesDataStore);

  Future<List<LocationWeatherModel>> getLocations() async {
    final locations = await preferencesDataStore.getLocations();
    List<LocationWeatherModel> locationsWeather = [];
    for (var locationName in locations) {
      locationsWeather.add(await getLocation(locationName));
    }

    return locationsWeather;
  }

  Future<LocationWeatherModel> getLocation(String location) async {
    final locationModel = await api.getLocationWeatherByCityName(location);

    return locationModel;
  }

  Future saveNewLocationName(String location) async {
    await preferencesDataStore.newLocation(location);
  }
}

@Injectable()
class SharedPreferencesDataStore {
  final SharedPreferences preferences;

  static const locationsKey = 'locationsSPKey';

  SharedPreferencesDataStore(this.preferences);

  Future<List<String>> getLocations() async {
    return preferences.getStringList(locationsKey) ?? [];
  }

  Future newLocation(String location) async {
    List<String> locations = preferences.getStringList(locationsKey) ?? [];

    locations.add(location);

    await preferences.setStringList(locationsKey, locations);
  }
}
