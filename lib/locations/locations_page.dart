import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:sun_day/core/di/injector.dart';
import 'package:sun_day/locations/locations_page_cubit.dart';
import 'package:sun_day/weather_details/weather_details_page.dart';

class LocationsPage extends StatefulWidget {
  const LocationsPage({Key? key}) : super(key: key);

  @override
  State<LocationsPage> createState() => _LocationsPageState();
}

class _LocationsPageState extends State<LocationsPage> {
  final LocationsPageCubit cubit = getIt.get<LocationsPageCubit>();

  @override
  void initState() {
    super.initState();
    cubit.onInitState();
  }

  @override
  @override
  void dispose() {
    cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationsPageCubit, LocationsPageState>(
        bloc: cubit,
        builder: (context, state) {
          Widget display = const CircularProgressIndicator.adaptive();

          if (state is LocationsPageLoadedState) {
            if (state.uiModel.locations.isNotEmpty) {
              final locationsList = state.uiModel.locations;
              display = ListView.separated(
                padding: const EdgeInsets.only(top: 16, bottom: 16),
                itemCount: locationsList.length,
                separatorBuilder: (BuildContext context, int index) {
                  return const Divider(
                    height: 0,
                  );
                },
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    onTap: () {
                      context.goNamed(
                        WeatherDetailsPage.kRouteName,
                        pathParameters: {WeatherDetailsPage.kRouteParamsLocation: locationsList[index].name},
                      );
                    },
                    title: Text(locationsList[index].name),
                    subtitle: SizedBox(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            locationsList[index].maxTemp,
                            style: const TextStyle(fontSize: 20),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            locationsList[index].minTemp,
                            style: const TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                    trailing: Text(
                      locationsList[index].currentTemp,
                      style: const TextStyle(fontSize: 24),
                    ),
                    leading: Image.network(
                      locationsList[index].icon,
                      height: 50,
                      width: 50,
                    ),
                  );
                },
              );
            } else {
              display = InkWell(
                onTap: () {
                  _showAddNewLocationDialog();
                },
                child: const Text('Tap to add a new location'),
              );
            }
          }

          return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Spacer(
                  flex: 5,
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 16.0),
                  child: Text(
                    'Locations',
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                ),
                if (state is LocationsPageLoadingState) const LinearProgressIndicator(),
                Expanded(
                  flex: 85,
                  child: Center(child: display),
                ),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                _showAddNewLocationDialog();
              },
              child: const Icon(
                Icons.add,
              ),
            ),
          );
        });
  }

  Future<void> _showAddNewLocationDialog() async {
    TextEditingController controller = TextEditingController();

    // Show dialog
    await showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Add a new location'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                  controller: controller,
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

    if (controller.text.isNotEmpty) {
      cubit.onAddNewCity(controller.text);
    }
  }
}
