import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:injectable/injectable.dart';
import 'package:sun_day/locations/locations_page.dart';
import 'package:sun_day/weather_details/weather_details_page.dart';

@Singleton()
class AppNavigatorProvider {
  static final GlobalKey<NavigatorState> rootNavigatorKey = GlobalKey<NavigatorState>();

  final GoRouter _router = GoRouter(
    navigatorKey: rootNavigatorKey,
    initialLocation: '/',
    routes: <RouteBase>[
      GoRoute(
        name: 'locations',
        path: '/',
        pageBuilder: (context, state) => const NoTransitionPage(
          child: LocationsPage(),
        ),
        routes: [
          GoRoute(
            name: WeatherDetailsPage.kRouteName,
            path: WeatherDetailsPage.kRoutePath,
            pageBuilder: (context, state) => MaterialPage(
              child: WeatherDetailsPage(location: state.pathParameters[WeatherDetailsPage.kRouteParamsLocation] ?? ''),
            ),
            routes: const [],
          )
        ],
      )
    ],
  );

  GoRouter provideRouter() => _router;
}
