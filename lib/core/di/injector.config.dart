// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:intl/intl.dart' as _i4;
import 'package:location/location.dart' as _i7;
import 'package:shared_preferences/shared_preferences.dart' as _i10;

import '../../locations/locations_page_cubit.dart' as _i14;
import '../../locations/locations_repository.dart' as _i11;
import '../../locations/locations_weather_api.dart' as _i8;
import '../../locations/shared_preferences_module.dart' as _i18;
import '../../weather_details/current_weather_api.dart' as _i13;
import '../../weather_details/date_format_module.dart' as _i15;
import '../../weather_details/following_days_weather_api.dart' as _i9;
import '../../weather_details/location_module.dart' as _i17;
import '../../weather_details/today_weather_api.dart' as _i12;
import '../app_navigator_provider.dart' as _i3;
import '../error_handler.dart' as _i6;
import 'dio_module.dart' as _i16;

// initializes the registration of main-scope dependencies inside of GetIt
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final dateFormatModule = _$DateFormatModule();
  final dioModule = _$DioModule();
  final locationModule = _$LocationModule();
  final sharedPreferencesModule = _$SharedPreferencesModule();
  gh.singleton<_i3.AppNavigatorProvider>(_i3.AppNavigatorProvider());
  gh.factory<_i4.DateFormat>(() => dateFormatModule.dateFormat);
  gh.factory<_i5.Dio>(() => dioModule.getDio());
  gh.singleton<_i6.ErrorHandler>(_i6.ErrorHandler());
  gh.factory<_i7.Location>(() => locationModule.location);
  gh.factory<_i8.LocationWeatherModelMapper>(
      () => _i8.LocationWeatherModelMapper());
  gh.factory<_i8.LocationsWeatherApi>(() => _i8.LocationsWeatherApi(
        gh<_i8.LocationWeatherModelMapper>(),
        gh<_i5.Dio>(),
      ));
  gh.factory<_i9.MultipleDaysWeatherMapper>(
      () => _i9.MultipleDaysWeatherMapper());
  await gh.factoryAsync<_i10.SharedPreferences>(
    () => sharedPreferencesModule.sharedPreferences,
    preResolve: true,
  );
  gh.factory<_i11.SharedPreferencesDataStore>(
      () => _i11.SharedPreferencesDataStore(gh<_i10.SharedPreferences>()));
  gh.factory<_i12.TodayWeatherMapper>(() => _i12.TodayWeatherMapper());
  gh.factory<_i13.CurrentWeatherMapper>(
      () => _i13.CurrentWeatherMapper(gh<_i4.DateFormat>()));
  gh.factory<_i9.FollowingDaysWeatherApi>(() => _i9.FollowingDaysWeatherApi(
        gh<_i7.Location>(),
        gh<_i5.Dio>(),
        gh<_i9.MultipleDaysWeatherMapper>(),
      ));
  gh.factory<_i11.LocationsRepository>(() => _i11.LocationsRepository(
        gh<_i8.LocationsWeatherApi>(),
        gh<_i11.SharedPreferencesDataStore>(),
      ));
  gh.factory<_i12.TodayWeatherApi>(() => _i12.TodayWeatherApi(
        gh<_i7.Location>(),
        gh<_i5.Dio>(),
        gh<_i12.TodayWeatherMapper>(),
      ));
  gh.factory<_i13.WeatherApi>(() => _i13.WeatherApi(
        gh<_i7.Location>(),
        gh<_i5.Dio>(),
        gh<_i13.CurrentWeatherMapper>(),
      ));
  gh.factory<_i14.LocationsPageCubit>(
      () => _i14.LocationsPageCubit(gh<_i11.LocationsRepository>()));
  return getIt;
}

class _$DateFormatModule extends _i15.DateFormatModule {}

class _$DioModule extends _i16.DioModule {}

class _$LocationModule extends _i17.LocationModule {}

class _$SharedPreferencesModule extends _i18.SharedPreferencesModule {}
