import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:sun_day/core/di/injector.dart';
import 'main_common.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies(const Environment(Environment.dev));

  runApp(const MyApp());
}

